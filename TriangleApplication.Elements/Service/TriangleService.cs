﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleApplication.Elements.Enum;

namespace TriangleApplication.Elements.Service
{
    public class TriangleService
    {
        /// <summary>
        /// Determines the type of triangle based on the given triangle sides.
        /// </summary>
        /// <param name="firstTriangleSide">The first triangle side.</param>
        /// <param name="secondTriangleSide">The second triangle side.</param>
        /// <param name="thirdTriangleSide">The last triangle side.</param>
        /// <exception cref="ArgumentException">Thrown if any triangle sides are of value zero or below.</exception>
        /// <returns>The type of the triangle.</returns>
        public TriangleType CalculateTriangleType(
            double firstTriangleSide,
            double secondTriangleSide,
            double thirdTriangleSide)
        {
            if (firstTriangleSide <= 0 || secondTriangleSide <= 0 || thirdTriangleSide <= 0)
            {
                throw new ArgumentException();
            }

            var uniqueTriangleSides = new HashSet<double>(new[] {
                firstTriangleSide,
                secondTriangleSide,
                thirdTriangleSide });

            return (TriangleType)uniqueTriangleSides.Count;
        }
    }
}
