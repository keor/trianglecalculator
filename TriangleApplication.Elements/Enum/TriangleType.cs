﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleApplication.Elements.Enum
{
    public enum TriangleType
    {
        Equilateral = 1,
        Isosceles = 2,
        Scalene = 3
    }
}
