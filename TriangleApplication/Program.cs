﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleApplication.Elements.Service;

namespace TriangleApplication
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var triangleService = new TriangleService();

            Console.WriteLine(
                    "We will now determine the type of a given triangle, based on your inputs. \n" +
                    "You are only allowed to enter numeric values. Decimals are allowed.");

            var firstTriangleSide = PromptUserForTriangleSide(
                promptMessage: "Please enter the first side of the triangle: ");

            var secondTriangleSide = PromptUserForTriangleSide(
                promptMessage: "Please enter the second side of the triangle: ");

            var thirdTriangleSide = PromptUserForTriangleSide(
                promptMessage: "Please enter the last side of the triangle: ");

            try
            {
                var triangleType = triangleService.CalculateTriangleType(
                    firstTriangleSide,
                    secondTriangleSide,
                    thirdTriangleSide);

                Console.WriteLine($"Based on your input, your triangle is a: {triangleType}!");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Some of the given triangle sides are invalid.");
            }
            catch (Exception)
            {
                Console.WriteLine("Something went terribly wrong.");
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private static double PromptUserForTriangleSide(string promptMessage)
        {
            double triangleSide;
            string input;

            do
            {
                Console.Write(promptMessage);
                input = Console.ReadLine();
            } while (double.TryParse(input, out triangleSide) == false);

            return triangleSide;
        }
    }
}
