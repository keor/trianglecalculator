﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriangleApplication.Elements.Service;
using TriangleApplication.Elements.Enum;

namespace TriangleApplication.Test
{
    [TestClass]
    public class TriangleValidationTests
    {
        private TriangleService _triangleService;

        [TestInitialize]
        public void Setup()
        {
            _triangleService = new TriangleService();
        }

        [TestMethod]
        public void CalculateTriangle_ValidSideLengths_IsTriangle()
        {
            var triangleSideOne = 10D;
            var triangleSideTwo = 25.5D;
            var triangleSideThree = 15.5D;

            var result = _triangleService.CalculateTriangleType(triangleSideOne, triangleSideTwo, triangleSideThree);

            Assert.IsInstanceOfType(result, typeof(TriangleType));
        }

        [TestMethod]
        public void CalculateTriangle_OneLengthIsZero_ThrowException()
        {
            var triangleSideOne = 0D;
            var triangleSideTwo = 25.5D;
            var triangleSideThree = 15.5D;

            try
            {
                var result = _triangleService.CalculateTriangleType(triangleSideOne, triangleSideTwo, triangleSideThree);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
                // Invalid triangle side lengths
            }
        }

        [TestMethod]
        public void CalculateTriangle_OneLengthIsBelowZero_ThrowException()
        {
            var triangleSideOne = -10D;
            var triangleSideTwo = 25.5D;
            var triangleSideThree = 15.5D;

            try
            {
                var result = _triangleService.CalculateTriangleType(triangleSideOne, triangleSideTwo, triangleSideThree);
                Assert.Fail();
            }
            catch (ArgumentException)
            {
                // Invalid triangle side lengths
            }
        }

        [TestMethod]
        public void CalculateTriangle_ThreeIdenticalLengths_IsEquilateral()
        {
            var triangleSideOne = 30D;
            var triangleSideTwo = 30D;
            var triangleSideThree = 30D;

            var result = _triangleService.CalculateTriangleType(triangleSideOne, triangleSideTwo, triangleSideThree);

            Assert.AreEqual(result, TriangleType.Equilateral);
        }

        [TestMethod]
        public void CalculateTriangle_TwoIdenticalLengths_IsNotEquilateral()
        {
            var triangleSideOne = 30D;
            var triangleSideTwo = 30D;
            var triangleSideThree = 35.5D;

            var result = _triangleService.CalculateTriangleType(triangleSideOne, triangleSideTwo, triangleSideThree);

            Assert.AreNotEqual(result, TriangleType.Equilateral);
        }

        [TestMethod]
        public void CalculateTriangle_TwoIdenticalLengths_IsIsosceles()
        {
            var triangleSideOne = 30D;
            var triangleSideTwo = 30D;
            var triangleSideThree = 35.5D;

            var result = _triangleService.CalculateTriangleType(triangleSideOne, triangleSideTwo, triangleSideThree);

            Assert.AreEqual(result, TriangleType.Isosceles);
        }

        [TestMethod]
        public void CalculateTriangle_NoIdenticalLengths_IsScalene()
        {
            var triangleSideOne = 30D;
            var triangleSideTwo = 39D;
            var triangleSideThree = 35.5D;

            var result = _triangleService.CalculateTriangleType(triangleSideOne, triangleSideTwo, triangleSideThree);

            Assert.AreEqual(result, TriangleType.Scalene);
        }
    }
}
